//Contoh Conditional Variable
const nilai = 75;
const status = nilai >= 70 ? "Lulus" : "Tidak Lulus";
console.log(status); // Lulus

//Contoh Map
const listSiswa = [
  {
    nama: "Garok",
    nilai: 85,
  },
  {
    nama: "Raben",
    nilai: 69,
  },
  {
    nama: "Luffy",
    nilai: 78,
  },
];

const listNamaSiswa = listSiswa.map((siswa) => siswa.nama);
console.log(listNamaSiswa);

//Contoh Filter
const listSiswaLulus = listSiswa.filter((siswa) => siswa.nilai >= 70);
console.log(listSiswaLulus);

//Menghitung Diskon pada Produk
const listProduk = [
  {
    nama: "Kemeja",
    harga: 200000,
    diskon: 0.1, // 10% diskon
  },
  {
    nama: "Sepatu",
    harga: 500000,
    diskon: 0.2, // 20% diskon
  },
  {
    nama: "Celana",
    harga: 150000,
    diskon: 0, // Tidak ada diskon
  },
];

const produkDiskon = listProduk.map((produk) => ({
  ...produk,
  hargaDiskon: produk.harga - produk.harga * produk.diskon,
}));

console.log("Produk dengan Diskon:", produkDiskon);

//Mengelompokkan Angka Genap dan Ganjil
const angka = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const angkaGenap = angka.filter((num) => num % 2 === 0);
const angkaGanjil = angka.filter((num) => num % 2 !== 0);

console.log("Angka Genap:", angkaGenap);
console.log("Angka Ganjil:", angkaGanjil);
